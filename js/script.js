// NAVBAR

const navSlide = ()=>{
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav-links');

    burger.addEventListener('click', ()=>{
        nav.classList.toggle('nav-active');
    });
}

navSlide();

// $("#tile-1 .nav-tabs a").click(function() {
//   var position = $(this).parent().position();
//   var width = $(this).parent().width();
//     $("#tile-1 .slider").css({"left":+ position.left,"width":width});
// });
// var actWidth = $("#tile-1 .nav-tabs").find(".active").parent("li").width();
// var actPosition = $("#tile-1 .nav-tabs .active").position();
// $("#tile-1 .slider").css({"left":+ actPosition.left,"width": actWidth});


// // ---------IMAGE UPLOAD-----------

// $(function() {
//     $("#fileupload").change(function(event) {
//         var x = URL.createObjectURL(event.target.files[0]);
//         $("#upload-img").attr("src", x);
//         console.log(event);
//     });
// })


// const dropZone = document.querySelector(".drop-zone");
// const browseBtn = document.querySelector(".browseBtn");
// const fileInput = document.querySelector("#fileInput");
// const host = "https://innshare.herokuapp.com/";
// const uploadURL = `${host}api/files`;

// dropZone.addEventListener("dragover", (e) => {
//     e.preventDefault();
//     if (!dropZone.classList.contains("dragged")) {
//         dropZone.classList.add("dragged");
//     }
// });

// dropZone.addEventListener("dragleave", () => {

//     dropZone.classList.remove("dragged");

// });

// dropZone.addEventListener("drop", (e) => {

//     e.preventDefault();
//     dropZone.classList.remove("dragged");
//     const files = e.dataTransfer.files;
//     console.table(files);
//     if (files.length) {
//         fileInput.files = files;
//         uploadFile()
//     }
// });

// browseBtn.addEventListener("click", () => {
//     fileInput.click();
// });

// const uploadFile = () => {
//     const file = fileInput.files[0];
//     const formData = new FormData();
//     formData.append("myfile", file);

//     const xhr = new XMLHttpRequest();
//     xhr.onreadystatechange = () => {
//         if (xhr.readyState === XMLHttpRequest.DONE) {
//             console.log(xhr.readyState);
//         }
//     };
//     xhr.open("POST", uploadURL);
//     xhr.send(formData);
// };

$(function(){
    $("#fileupload").change(function(event)
    {
        var x=URL.createObjectURL(event.target.files[0]);
        $("#upload-img").attr("src",x);
        console.log(event);
    });
  })
  var s1=window.innerWidth;
  var s2=window.innerHeight;
  console.log(window.innerWidth);
  console.log(window.innerHeight);
  
  
  // Loaded via <script> tag, create shortcut to access PDF.js exports.
  var pdfjsLib = window['pdfjs-dist/build/pdf'];
  // The workerSrc property shall be specified.
  pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://mozilla.github.io/pdf.js/build/pdf.worker.js';
  var scale;
  $("#myPdf").on("change", function(e){
      var file = e.target.files[0]
      if(file.type == "application/pdf"){
          var fileReader = new FileReader();  
          fileReader.onload = function() {
              var pdfData = new Uint8Array(this.result);
              // Using DocumentInitParameters object to load binary data.
              var loadingTask = pdfjsLib.getDocument({data: pdfData});
              loadingTask.promise.then(function(pdf) {
                console.log('PDF loaded');
                
                // Fetch the first page
                var pageNumber = 1;
                pdf.getPage(pageNumber).then(function(page) {
                  console.log('Page loaded');
              
                  
                  if(s1<=500)
                       scale =.5;  
                  else 
                       scale=.8;
                  console.log(scale);
                  // .8
                  var viewport = page.getViewport({scale: scale});
  
                  // Prepare canvas using PDF page dimensions
                  var canvas = $("#pdfViewer")[0];
                  var context = canvas.getContext('2d');
                  canvas.height = viewport.height;
                  canvas.width = viewport.width;
  
                  // Render PDF page into canvas context
                  var renderContext = {
                    canvasContext: context,
                    viewport: viewport
                  };
                  var renderTask = page.render(renderContext);
                  renderTask.promise.then(function () {
                    console.log('Page rendered');
                  });
                });
              }, function (reason) {
                // PDF loading error
                console.error(reason);
              });
          };
          fileReader.readAsArrayBuffer(file);
      }
  });
  